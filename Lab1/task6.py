import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.svm import OneClassSVM
from sklearn.metrics import accuracy_score

from utilities import visualize_classifier  # Assuming you have a function for visualization

# Load your data
input_file = 'data_multivar_nb.txt'
data = np.loadtxt(input_file, delimiter=',')
X, y = data[:, :-1], data[:, -1]

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=3)

# Create and train the One-Class SVM classifier
classifier_new = OneClassSVM()
classifier_new.fit(X_train)

# Predict the labels for the test set
y_test_pred = classifier_new.predict(X_test)

# Evaluate the classifier
accuracy = accuracy_score(y_test, y_test_pred)
print("Accuracy of the new classifier =", round(accuracy * 100, 2), "%")

# Visualize the classifier's decision boundary (if applicable)
visualize_classifier(classifier_new, X_test, y_test)  # You may need to modify this based on your visualization function
