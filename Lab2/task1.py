import numpy as np
from sklearn import preprocessing
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsOneClassifier
from sklearn.model_selection import train_test_split, cross_val_score
import warnings

# Suppress FutureWarning for LinearSVC
warnings.simplefilter(action='ignore', category=FutureWarning)

input_file = 'income_data.txt'

X = []
y = []
count_class1 = 0
count_class2 = 0
max_datapoints = 25000

with open(input_file, 'r') as f:
    for line in f.readlines():
        if count_class1 >= max_datapoints and count_class2 >= max_datapoints:
            break

        if '?' in line:
            continue

        data = line[:-1].split(', ')

        if data[-1] == '<=50K' and count_class1 < max_datapoints:
            X.append(data)
            count_class1 += 1

        if data[-1] == '>50K' and count_class2 < max_datapoints:
            X.append(data)
            count_class2 += 1

X = np.array(X)

label_encoder = []
X_encoded = np.empty(X.shape)

for i, item in enumerate(X[0]):
    if item.isdigit():
        X_encoded[:, i] = X[:, i]
    else:
        label_encoder.append(preprocessing.LabelEncoder())
        X_encoded[:, i] = label_encoder[-1].fit_transform(X[:, i])

X = X_encoded[:, :-1].astype('int')
y = X_encoded[:, -1].astype('int')

# Suppress FutureWarning for OneVsOneClassifier
warnings.simplefilter(action='ignore', category=FutureWarning)

# Explicitly set dual=False to suppress FutureWarning
classifier = OneVsOneClassifier(LinearSVC(random_state=0, dual=False))

# Fit the model on the entire dataset
classifier.fit(X, y)

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

# Fit the model on the training data
classifier.fit(X_train, y_train)

# Make predictions on the test set
y_test_pred = classifier.predict(X_test)

# Evaluate the model using cross-validation
f1 = cross_val_score(classifier, X, y, scoring='f1_weighted', cv=3)
accuracy_values = cross_val_score(classifier, X, y, scoring='accuracy', cv=3)
precision_values = cross_val_score(classifier, X, y, scoring='precision_weighted', cv=3)
recall_values = cross_val_score(classifier, X, y, scoring='recall_weighted', cv=3)

# Print evaluation metrics
print("F1 score:", round(100 * f1.mean(), 2), "%")
print("Accuracy:", round(100 * accuracy_values.mean(), 2), "%")
print("Precision:", round(100 * precision_values.mean(), 2), "%")
print("Recall:", round(100 * recall_values.mean(), 2), "%")

# Make a prediction on a new data point
input_data = ['37', 'Private', '215646', 'HS-grad', '9', 'Never-married', 'Handlers-cleaners', 'Not-in-family', 'White', 'Male', '0', '0', '40', 'United-States']
input_data_encoded = [-1] * len(input_data)

count = 0
for i, item in enumerate(input_data):
    if item.isdigit():
        input_data_encoded[i] = int(input_data[i])
    else:
        input_data_encoded[i] = int(label_encoder[count].transform([input_data[i]])[0])
        count += 1

input_data_encoded = np.array(input_data_encoded).reshape(1, -1)

# Predict the class and print the result
predicted_class = classifier.predict(input_data_encoded)
print("Predicted income class:", label_encoder[-1].inverse_transform(predicted_class)[0])
